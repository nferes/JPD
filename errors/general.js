class MissingParameter extends Error {
    constructor(message) {
        super(message);
        this.code = 422;
    }
}

class ElementNotFound extends Error {
    constructor(message) {
        super(message);
        this.code = 404;
    }
}

class MalformedParameter extends Error {
    constructor(message) {
        super(message);
        this.code = 400;
    }
}

class UknownError extends Error {
    constructor(message) {
        super(message);
        this.code = 500;
    }
}

module.exports = {
    MissingParameter,
    ElementNotFound,
    MalformedParameter,
    UknownError,
};






