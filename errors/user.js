                                                   

class UserAlreadyExist extends Error {
    constructor(message) {
        super(message);
        this.code = 409;
    }
}
class YouAreNotAdmin extends Error {
    constructor(message) {
        super(message);
        this.code = 401;
    }
}

module.exports = {
    UserAlreadyExist,
    YouAreNotAdmin
};




