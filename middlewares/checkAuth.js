const JWT = require('jsonwebtoken');
const ENV = require('../models/ENV');
const { getUser } = require('../repositories/UserQuery')


module.exports = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const payload = JWT.decode(token, ENV.JWT.JWT_SECRET);

        
        const user = await getUser(payload.id);
        req.user = user;
        next();
    }   catch (e) {
        return res.status(401).json({
            error: true,
            message: 'Unauthorized Access'
        });
    }
};