const bcrypt = require('bcrypt')
const { ObjectId } = require('mongoose').Types;
const FhcieClient = require('../models/FicheClient');
const {
    MissingParameter,
    MalformedParameter,
    UknownError,
} = require('../errors/general');

exports.addFiche = (entrepriseId, date, compteRendu, userId) => new Promise((resolve, reject)=>{
    
    if(date === undefined){
        return reject(new MissingParameter('the date is a required field'));
    }
    if(!ObjectId.isValid(entrepriseId)){
        return reject(new MalformedParameter('the entreprise ID does not respect id format'));
    }
    
    if(!ObjectId.isValid(userId)){
        return reject(new MalformedParameter('userId does not respect id format'));
    }
    
    const ficheClient = new FhcieClient();
    ficheClient.user.push(userId);
    ficheClient.entreprise.push(entrepriseId);
    ficheClient.date.push(date);
    ficheClient.compteRendu.push(compteRendu)
    return ficheClient
        .save()
        .then(()=> resolve(ficheClient))
        .catch((e)=>{
            return reject(new UknownError(e.message))
        })
})

