const bcrypt = require('bcrypt')
const { ObjectId } = require('mongoose').Types;
const User = require('../models/User');
const ENV = require('../models/ENV');
const {
    MissingParameter,
    ElementNotFound,
    MalformedParameter,
    UknownError,
} = require('../errors/general');
const { UserAlreadyExist} = require('../errors/user');

exports.login = (username, password) => new Promise((resolve, reject)=>{
    if(username === undefined){
        return reject(new MissingParameter('username is a required field'));
    }
    if(password === undefined) {
        return reject(new MissingParameter('password is a required field'));
    }
    return User.findOne({
        username,
    })
    .exec()
    .then((user)=>{
        if(user) {
            if(bcrypt.compareSync(password, user.password)){
                return resolve(user)
            }
            return reject(new ElementNotFound('Wrong username/password '));
        }
        return reject(new ElementNotFound(`There is np user using the username ${username}`));
    })
    .catch(e =>{
        console.log(e)
        reject(new UknownError(e.message));
    })
})

exports.registerad = (username, password) => new Promise((resolve, reject) => {
    if(username === undefined){
        return reject(new MissingParameter('Username is a required field'));
    }
    if(password === undefined){
        return reject(new MissingParameter('password is a required field'));
    }
    const user  = new User();
    user.username = username;
    user.password = bcrypt.hashSync(password, ENV.Salt);
    user.role = "ROLE_RESPONSABLE";
    return user
        .save()
        .then(()=> resolve(user))
        .catch((e)=>{
            if(e.code === 11000){
                return reject(new UserAlreadyExist(`${username} already exists`));
            }
            return reject(new UknownError(e.message));
        })
})
exports.register = (username, password, Objectif_De_Chiffre) => new Promise((resolve, reject) => {
    if(username === undefined){
        return reject(new MissingParameter('Username is a required field'));
    }
    if(password === undefined){
        return reject(new MissingParameter('password is a required field'));
    }
    const user  = new User();
    user.username = username;
    user.password = bcrypt.hashSync(password, ENV.Salt);
    user.role = "ROLE_COMMERCIAL";
    user.Objectif_De_Chiffre = Objectif_De_Chiffre;
    user.Montant_Signe =0;
    return user
        .save()
        .then(()=> resolve(user))
        .catch((e)=>{
            if(e.code === 11000){
                return reject(new UserAlreadyExist(`${username} already exists`));
            }
            return reject(new UknownError(e.message));
        })
})
exports.getUser = userId => new Promise((resolve, reject)=>{
    if(userId === undefined) {
        return reject(new MissingParameter('userId is a required field'))
    }
    if (!ObjectId.isValid(userId)) {
        return reject(new MalformedParameter('userId does not respect id format'));
    }
    return User.findById(userId)
        .exec()
        .then((user) => {
            if(user) {
                return resolve(user);
            }
            return reject(new ElementNotFound(`there is no user with ID (${userId})`));
        })
        .catch(e => reject(new UknownError(e.message)));
});

exports.getUsers = ()=> new Promise((resolve, reject)=>{
    return User.find({
        role: "ROLE_COMMERCIAL"
    })
        .select('-password')
        .exec()
        .then(resolve)
        .catch(e => reject(new UknownError(e.message)));
})

exports.getUserByName = (username) => new Promise((resolve, reject)=>{
    if(username === undefined){
        return reject(new MissingParameter('name is a required field'));
    }
    return User.find({
        username: username
    })
        .exec()
        .then(resolve)
        .catch(e => reject(new UknownError(e.message)))
})