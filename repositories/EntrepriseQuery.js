const Entreprise = require('../models/Entreprise');
const {ObjectId} = require('mongoose').Types;
const {
    MissingParameter,
    ElementNotFound,
    MalformedParameter,
    UnknownError,
} = require('../errors/general');
const { EntrepriseAlreadyExist} = require('../errors/entreprise')




exports.addEntreprise = (nom, adresse, tel, userId, type, mail, visavis, mailvisavis, telvisavis, postevisavis, username) => new Promise((resolve, reject)=>{
    
    if(nom === undefined){
        return reject(new MissingParameter('the name is a required field'));
    }
    if(adresse === undefined){
        return reject(new MissingParameter('the address is a required field'));
    }
    if(tel === undefined){
        return reject(new MissingParameter('the phone number is a required field'));
    }
    if(!ObjectId.isValid(userId)){
        return reject(new MalformedParameter('userId does not respect id format'));
    }
    if(type !== "PME" && type !== "Startup" && type !== "Incubateur" && type !== "TPE" && type !== "TGE" && type !== "Multinationale" && type !== "Banque")
        return reject( new ElementNotFound(`there is no type <ith the name : ${type}`))
    
    const entreprise = new Entreprise();
    entreprise.nom = nom;
    entreprise.adresse = adresse;
    entreprise.Tel = tel;
    entreprise.type = type;
    entreprise.user = userId
    entreprise.mail = mail;
    entreprise.visavis = visavis;
    entreprise.mailvisavis = mailvisavis;
    entreprise.telvisavis = telvisavis;
    entreprise.postevisavis = postevisavis;
    entreprise.username = username;
    return entreprise
        .save()
        .then(()=> resolve(entreprise))
        .catch((e)=>{
            if(e.code === 11000){
                return reject(new EntrepriseAlreadyExist(e.message) )
            }
        })
})

exports.getEntreprise = entrepriseId => new Promise((resolve, reject)=>{
    if(entrepriseId === undefined){
        return reject(new MissingParameter('entreprise ID is a required field'));
    }
    if(!ObjectId.isValid(entrepriseId)){
        return reject(new MalformedParameter('Entreprise ID does not respect id format'))
    }
    return Entreprise.findById(entrepriseId)
        .exec()
        .then((entreprise) =>{
            if(entreprise){
                return resolve(entreprise)
            }
            return reject(new ElementNotFound(`there is no entreprise with ID: ${entrepriseId}`))
        })
})
exports.getEntreprises = ()=> new Promise((resolve, reject)=>{
    return Entreprise.find({})
            .exec()
            .then(resolve)
            .catch(e => reject(new UnknownError(e.message)));
});
exports.getEntrepriseByName = (name) => new Promise((resolve, reject)=>{
    if(name === undefined)
        return reject(new MissingParameter('name is a requried field'));
    return Entreprise.find({
        nom: name
    })
    .exec()
    .then(resolve)
    .catch(e => reject(new UnknownError(e.message)));
})