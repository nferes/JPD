const Dispatching = require('../models/dispatching');
const {ObjectId} = require('mongoose').Types;
const {
    MissingParameter,
    ElementNotFound,
    MalformedParameter,
    UnknownError
} = require('../errors/general');

exports.addDispatching = (date,userId, entrepriseId, deadline, tache, commentaire ) => new Promise((resolve, reject)=>{
    if(date === undefined)
        return reject(new MissingParameter('the date is a required field'))
    if(!ObjectId.isValid(userId))
        return reject(new MalformedParameter('userId does not respect the ID format'));
    if(!ObjectId.isValid(entrepriseId))
        return reject(new MalformedParameter('the entreprise ID does not respect the ID format'));
    if(deadline === undefined)
        return reject(new MissingParameter('the deadline is a required field'))
    const dispatching = new Dispatching();
    console.log(dispatching)
    
    //testdate = new Date('2011-04-11');
    //testdeadline = new Date('2011-04-11');
    dispatching.date = new Date(date);
    dispatching.users = userId;
    dispatching.entreprise = entrepriseId;
    dispatching.deadline = new Date(deadline);
    if(tache !== undefined)
        dispatching.tache = tache;
    if(commentaire !== undefined)    
        dispatching.commentaire = commentaire;
    return dispatching
        .save()
        .then(()=>resolve(dispatching))
        .catch((e)=>{
                return reject(new UnknownError(e.message))
            
        })
})
exports.getDispatching = dispatchingId => new Promise((resolve, reject)=>{
    if(!ObjectId.isValid(dispatchingId)){
        return reject(new MalformedParameter('Dipatching ID does not respect the ID format'))
    }
    return Dispatching.findById(dispatchingId)
        .exec()
        .then((dispatching) =>{
            if(dispatching) return resolve(dispatching);
            return reject(new ElementNotFound(`there is no dispatching with ID: ${dispatchingId}`))
        })
})
exports.getDisptchings = () => new Promise((resolve, reject)=>{
    return Dispatching.find()
            .exec()
            .then(resolve)
            .catch(e => reject (new UnknownError(e.message)))
})