const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/jpddb' ,{
    useNewUrlParser: true,
    useCreateIndex: true,
});

module.exports = mongoose;