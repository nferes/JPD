const bcrypt = require('bcrypt');

module.exports = {
    JWT: {
        JWT_SECRET: '6v9y$B&E)H@McQfTjWnZr4t7w!z%C*F-',
        JWT_EXP: '330h',
    },
    Salt: bcrypt.genSaltSync(10),
};