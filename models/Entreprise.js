const mongoose = require('./DB');

const entrepriseSchema = new mongoose.Schema({
    nom :{
        type: String,
        unique: true,
        required: true
    },
    adresse: {
        type: String,
    },
    Tel: {
        type: String
    },
    /*ficheClient: {
        type: mongoose.Schema.Types.ObjectId, ref: 'FicheClient'
    },*/
    user: 
        {
            type: mongoose.Schema.Types.ObjectId, ref: 'User'
        },
    username: {
        type: String
    },
    
    type: {
        type: String,
        required: true,
    },
    visavis: {
        type: String,
        required: true,
    },
    mailvisavis: {
        type: String
    },
    telvisavis: {
        type: String
    },
    postevisavis:{
        type: String
    },
    mail:{
        type:String
    }


},{
    createdAt: 'created_at',
    updatedAt: 'updated_at',
});
module.exports = mongoose.model('Entreprise', entrepriseSchema);