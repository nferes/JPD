const mongoose = require('./DB');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
    },
    Objectif_De_Chiffre: {
        type: Number,
       
    },
    Montant_Signe: {
        type: Number,
        
    }

}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
});

module.exports = mongoose.model('User', userSchema);