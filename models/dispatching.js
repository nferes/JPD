const mongoose = require('./DB');

const dispatchingSchema = new mongoose.Schema({
    date: {
        type: Date,
        required: true,
    },
    users: {
        type: mongoose.Schema.Types.ObjectId, ref: 'User',
    },
    entreprise: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Entreprise'
    },
    deadline: {
        type: Date,
        required: true,
    },
    tache: {
        type: String
    },
    commentaire: String
    
},{
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});
module.exports = mongoose.model('Dispatching', dispatchingSchema);