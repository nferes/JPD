const mongoose = require('./DB');

const fichClientSchema = new mongoose.Schema({
    RDVS: [
        {
            compteRendu: String,
            date: Date,
            user: {
                type: mongoose.Schema.Types.ObjectId, ref: 'User'
            },
            entreprise: {
                type: mongoose.Schema.Types.ObjectId, ref: 'Entreprise'
            }
        }
    ]
})

module.exports =  mongoose.model('FicheClient', fichClientSchema);
