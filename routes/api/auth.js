const express = require('express');
const JWT = require('jsonwebtoken');
const ENV = require('../../models/ENV');
const {login, registerad} = require('../../repositories/UserQuery')


const router = express.Router();

router.post('/login', async (req, res)=>{
    try {
        const {username, password } = req.body;
        const user = await login(username, password);
        const token = JWT.sign({
            id : user._id
        }, ENV.JWT.JWT_SECRET, {expiresIn: ENV.JWT.JWT_EXP});
        return res.status(200).json({token, ...user, message: 'good' })

    } catch (e){
        return res.status(e.code).json({ error: true, message: e.message})
    }
})

router.post('/register', async (req, res)=>{
    try {
        const {username, password} = req.body;
        const user = await registerad(username, password);
        return res.status(200).json(user)
    }catch(e) {
        return res.status(e.code).json({error: true, message: e.message});
    }
});

module.exports = router;