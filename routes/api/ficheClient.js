const express = require('express');
const checkAuth = require('../../middlewares/checkAuth')
const router = express.Router();

const {getEntrepriseByName} = require('../../repositories/EntrepriseQuery');
const {getUserByName} = require('../../repositories/UserQuery');
const {addFicheClient} = require('../../repositories/FicheClientQuery')


router.post('/',checkAuth, async(req,res)=>{
    try{
        const {
            username,
            entrerpisename,
            compteRenduPath,
            date,
        } = req.body;
        const user = await getUserByName(username)
        const entreprise = await getEntrepriseByName(entrerpisename)
        const ficheClient = await   addFicheClient(user[0].id, entrerpise[0].id, compteRenduPath, date)

        return res.status(200).json(ficheClient);
    } catch(e){
        return res.status(e.code).json({error: true, message: e.message})
    }
});


module.exports = router;