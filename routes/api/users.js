var express = require('express');
const bcrypt = require('bcrypt');
const CheckAuth = require('../../middlewares/checkAuth')
var router = express.Router();
const {
  register,
  getUsers,
  getUser
} = require('../../repositories/UserQuery')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/' ,CheckAuth,  async(req, res )=>{
  try {
    const {
      username,
      password,
      Objectif_De_Chiffre,
    } = req.body;
    const user = await register(username, password, Objectif_De_Chiffre);
    return res.status(200).json(user);
  } catch(e) {
    return res.status(e.code).json({error: true, message: e.message});
  }
});
router.get('/all',CheckAuth, async(req, res)=>{
  try{
    const users = await getUsers();
    return res.status(200).json(users);

  } catch (e) {
    return res.status(e.code).json({error: true, message: e.message})
  }
})

router.put('/:userId',CheckAuth, async (req, res)=> {
  try{
    const {userId} = req.params;
    const  {username, password, Objectif_De_Chiffre, Montant_Signe} = req.body;
    const user = await getUser(userId);
    if(username !== undefined)
    user.username = username;
    if(password !== undefined)
    user.password = bcrypt.hashSync(password, 10);
    if(Objectif_De_Chiffre !== undefined)
    user.Objectif_De_Chiffre = Objectif_De_Chiffre;
    if(Montant_Signe !== undefined)
    user.Montant_Signe = Montant_Signe;
    user.save();
    return res.status(200).json({error: false, message: `${user.username} has been updated`});
  } catch(e){
    return res.status(e.code).json({error: true, message: e.message})
  }
})
router.delete('/:userId', CheckAuth, async (req,res)=>{
  try{
    const {userId} = req.params;
    const user = await getUser(userId);
    user.delete();
    return res.status(200).json({error: false, message: `${user.username} has been deleted`})
  } catch (e) {
    return res.status(e.code).json({error: true, message: e.message})
  }
})
router.get('/:userid', CheckAuth, async (req,res)=>{
  try{
    const {userId} = req.params;
    const user = await getUser(userId);
    return res.status(200).json(user);
  } catch (e){
    return res.status(e.code).json({error: true, message: e.message})
  }
})
module.exports = router;
