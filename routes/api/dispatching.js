const express = require('express');
const checkAuth = require('../../middlewares/checkAuth');
const router = express.Router();

const {getUserByName} = require('../../repositories/UserQuery');
const {getEntrepriseByName} = require('../../repositories/EntrepriseQuery');
const {addDispatching, getDispatching, getDisptchings} = require('../../repositories/DispatchingQuery')
router.post('/', checkAuth, async(req,res)=>{
    try{
        const{
            date,
            username,
            entreprisename,
            deadline,
            tache,
            commentaire
        } = req.body;
        
        const user = await getUserByName(username);
        
        const entreprise = await getEntrepriseByName(entreprisename)
        const dispatching = await addDispatching(date,user[0]._id,entreprise[0]._id,deadline, tache, commentaire);
        
        return res.status(200).json(dispatching);
    } catch(e) {
        return res.status(e.code).json({error: true, message: e.message});
    }
})
router.get('/:dispatchingId', checkAuth, async(req, res)=>{
    try{
        const {dispatchingId} = req.params;
        const dispatching = await getDispatching(dispatchingId);
            return res.status(200).json(dispatching);
    } catch(e){
        return res.status(e.code).json({error: true, message: e.message});
    }
})
router.put('/:dispatchingId', checkAuth, async(req, res)=>{
    try {
        const{
            date,
            username,
            entreprisename,
            deadline,
            tache,
            commentaire
        } = req.body;
        const {dispatchingId} = req.params;
        const dispatching = await getDispatching(dispatchingId)
        if(username !== undefined){
            const user = await getUserByName(username);
            dispatching.users = user[0].id;
        }
        if(entreprisename !== undefined){
            const entreprise = await getEntrepriseByName(entreprisename);
            dispatching.entreprise = entreprise[0].id
        }
        if(date !== undefined) dispatching.date = date;
        if(deadline !== undefined) dispatching.deadline= deadline;
        if(tache !== undefined) dispatching.tache= tache;
        if(commentaire !== undefined) dispatching.commentaire = commentaire;
        dispatching.save()
        return res.status(200).json({error: false, message: `the dispatching of date: ${dispatching.date} has been updated`});
    } catch(e){
        return res.status(e.code).json({error:true, message: e.message})
    }
})
router.get('/', checkAuth, async (req,res)=>{
    try {
        const dispatchings = await getDisptchings();
        return res.status(200).json(dispatchings);
    } catch(e){
        return res.status(e.code).json({error: true, message: e.message})
    }
})

router.delete('/:dispatchingId', checkAuth, async (req,res)=>{
    try{
        const {dispatchingId} = req.params;
        const dispatching = await getDispatching(dispatchingId)
        dispatching.delete();
        return res.status(200).json({error: false, message: `Dispatching of the date: ${dispatching.date} has been deleted`});
    } catch(e){
        return res.status(e.code).json({error: true, message: e.message})
    }
})
module.exports = router;