    const express = require('express');
const JWT = require('jsonwebtoken');
const ENV = require('../../models/ENV')
const checkAuth = require('../../middlewares/checkAuth')
const objectAssign = require('object-assign');
const router = express.Router();

const {addEntreprise, getEntreprise, getEntreprises} = require('../../repositories/EntrepriseQuery');
const {getUser} = require('../../repositories/UserQuery');

router.get('/typesList', checkAuth, async(req, res)=>{
    try{
        return res.status(200).json({ list: ["PME", "Startup", "Incubateur" ,"TPE" ,"TGE" ,"Multinationale" , "Banque"]})
    } catch(e){
        return res.status(e.code).json({error: true, message: e.message})
    }
})
router.post('/',checkAuth, async(req,res)=>{
    try{
        const {
            nom,
            adresse,
            Tel,
            type,
            mail,
            visavis,
            mailvisavis,
            telvisavis,
            postevisavis
        } = req.body;
        const token = req.headers.authorization.split(' ')[1];
        const payload = JWT.decode(token, ENV.JWT.JWT_SECRET);
        const user = await getUser(payload.id)
        const entreprise = await addEntreprise(nom, adresse, Tel, payload.id, type, mail, visavis, mailvisavis, telvisavis, postevisavis, user.username);
        return res.status(200).json(entreprise);
    } catch(e){
        return res.status(e.code).json({error: true, message: e.message})
    }
});

router.put('/:entrepriseId', checkAuth,async (req,res)=>{
    try{
        const {entrepriseId} = req.params;
        const entreprise  = await getEntreprise(entrepriseId)
        const {nom, 
            adresse, 
            tel, 
            type,
            mail,
            visavis,
            mailvisavis,
            telvisavis,
            postevisavis} = req.body;
        if(type !== "PME" && type !== "Startup" && type !== "Incubateur" && type !== "TPE" && type !== "TGE" && type !== "Multinationale" && type !== "Banque")
            return res.status(400).json({error: true, message:`there is no type with name: ${type}`})
        if(nom !== undefined)
            entreprise.nom = nom;
        if(adresse !== undefined)
            entreprise.adresse = adresse;
        if(tel !== undefined)
            entreprise.tel = tel;
        if(type !== undefined)
            entreprise.type = type;
        if(visavis !== undefined)
            entreprise.visavis = visavis;
        if(mail !== undefined)
            entreprise.mail = mail;
        if(mailvisavis !== undefined)
            entreprise.mailvisavis = mailvisavis;
        if(telvisavis !== undefined)
            entreprise.telvisavis = telvisavis;
        if(postevisavis !== undefined)
            entreprise.postevisavis = postevisavis
        entreprise.save();
        return res.status(200).json({error: false, message: `${entreprise.nom} has been updated`})
    } catch(e){
        return res.status(e.code).json({error: true, message: e.message});
    }
})
router.get('/', checkAuth, async (req, res)=>{
    try {
        const entreprises = await getEntreprises();
         return   res.status(200).json(entreprises);
        
    } catch(e){
        return res.status(e.code).json({error: true, message: e.message})
    }
})
router.get('/:entrepriseId', checkAuth, async(req, res)=>{

    try {
    const {entrepriseId} = req.params;
    const entreprise = await getEntreprise(entrepriseId);
    return res.status(200).json(entreprise);
    } catch(e){
        return res.status(e.code).json({error: true, message: e.message})}
})
router.delete('/:entrepriseId', checkAuth, async (req,res)=>{
    try {
        const {entrepriseId} = req.params;
        const entreprise = await getEntreprise(entrepriseId);
        entreprise.delete();
        return res.status(200).json({error: false, message: `${entreprise.nom} has been deleted`});
    } catch(e) {
        return res.status(e.code).json({error: true, messsage: e.message})
    }
})


module.exports = router;